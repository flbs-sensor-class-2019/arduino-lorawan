![SensorSpace Logo](https://sensorspace.tech/images/logo1_vertical.png)

------------

### 1.) Install git and Arduino IDE

1. Install git https://git-scm.com/downloads

2. Install Arduino IDE https://www.arduino.cc/en/Main/Software

------------

### 2.) Setup Arduino IDE to use our Sodaq ExpLoRer board

1. Install the Sodaq board manager files

    Open up the Arduino IDE. Go to File->Preferences and enter the following URL in the **Additional Board Manager URLs** text box: `http://downloads.sodaq.net/package_sodaq_samd_index.json`. Then go to Tools->Board->Boards Manager and search for "Sodaq". Find **Sodaq SAMD Boards** and install the latest version (probably 1.6.20).

2. Select the board you will be building the code for

    Go to Tools->Board and select **Sodaq ExpLoRer**

------------

### 3.) Get the code + add libraries to Arduino IDE

1. Clone this project onto your desktop by right clicking on your desktop and clicking **Git Bash Here**. A terminal window will appear -- enter the command below in the terminal window.

    `git clone https://gitlab.com/flbs-sensor-class-2019/arduino-lorawan.git`

2. Open the Arduino IDE and then open the file **arduino-lorawan.ino** located in the project folder that you just cloned.

3. Copy the three libraries contained in the project folder (ArduinoLowPower, RTCZero, arduino-device-lib) into your Arduino libraries folder. This folder is located in C:\Users\YOUR USERNAME\Documents\Arduino\libraries.

------------
### 4.) Compile the code + upload it to the Sodaq ExpLoRer board

1. In the Arduino IDE, click Sketch->Verify/Compile to build the code. 

2. If all goes well, connect the board to your PC. Your computer should install any necessary drivers. Click Tools->Ports in the Arduino IDE and select the Sodaq Board -- it might appear as an Arduino Zero or a Sodaq ExpLoRer. Now click Sketch->Upload to upload the code to the board. 

------------


# The Things Network

Now you need to make an account on The Things Network: https://www.thethingsnetwork.org/

After you've signed up, create a new application here: https://console.thethingsnetwork.org/applications

Give the application any name you want and make sure to select **ttn-handler-us-west**!

![](https://i.ibb.co/dmxLsbb/image.png)

Then register a new device within your application. **This is an important step!** You need to get the **Device EUI** for your Sodaq Board. To do this, run the code that you compiled earlier on your Sodaq Board and open the Serial monitor in Arduino IDE (Tools->Serial Monitor). 
It should spit out a bunch of text and one of the lines of text will be the device EUI. Copy this and paste it into the **Device EUI** section shown below in red. Then click the Register button at the bottom.

![](https://i.ibb.co/QHnztq5/image.png)

Now your device is registered, but we need to do one last thing -- converting the floating point values received from the sensors on the Arduino into strings of text.

Go to your application and click **Payload Formats**. Set it to **Custom** and **Decoder** and paste this text into the text box below:

```
function Decoder(bytes, port) {

  // Based on https://stackoverflow.com/a/37471538 by Ilya Bursov
  function bytesToFloat(bytes) {
    var bits = bytes[3]<<24 | bytes[2]<<16 | bytes[1]<<8 | bytes[3];
    var sign = (bits>>>31 == 0) ? 1.0 : -1.0;
    var e = bits>>>23 & 0xff;
    var m = (e == 0) ? (bits & 0x7fffff)<<1 : (bits & 0x7fffff) | 0x800000;
    var f = sign * m * Math.pow(2, e - 150);
    return f;
  }  

  // Test with 0082d241 for 26.3135 
  return {
    // Take bytes 0 to 4 (not including), and convert to float:
    board_temperature: bytesToFloat(bytes.slice(0, 4)),
    battery_voltage: bytesToFloat(bytes.slice(4, 8))
  };
}
```

![](https://i.ibb.co/VYBfdHH/image.png)

Click the save button at the bottom.

Now go to the **Data** tab of your application, and every 2 minutes you should hopefully see data streaming in that looks something like this:

![](https://i.ibb.co/2hvb5Pk/image.png)

 